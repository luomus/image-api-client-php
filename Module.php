<?php
namespace ImageApi;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Image server module to handle communication with image api
 * @package ImageApi
 */
class Module implements ConfigProviderInterface
{
    /**
     * gets the configs for the module
     *
     * This contains the basic setting for the pdf created using this module
     *
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

}
