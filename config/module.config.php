<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'ImageApi\Controller\Image' => 'ImageApi\Controller\ImageController',
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'ImageService' => 'ImageApi\Service\ImageServiceFactory',
            'ImageApi\Service\Configuration' => 'ImageApi\Service\ConfigurationFactory'
        )
    )
);
