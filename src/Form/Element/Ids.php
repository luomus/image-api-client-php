<?php

namespace ImageApi\Form\Element;

use Kotka\Filter\StringToArray;
use Zend\Form\Element\Text;
use Zend\InputFilter\InputProviderInterface;

class Ids extends Text implements InputProviderInterface
{

    /**
     * @param  null|int|string $name Optional name for the element
     * @param  array $options Optional options for the element
     */
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
    }

    /**
     * Provide default input rules for this element
     *
     * @return array
     */
    public function getInputSpecification()
    {
        $filter = new StringToArray();
        $filter->setDelimiter(',');
        $spec = array(
            'name' => $this->getName(),
            'required' => true,
            'filters' => array(
                $filter
            )
        );

        return $spec;
    }

}