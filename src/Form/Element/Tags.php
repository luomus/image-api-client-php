<?php

namespace ImageApi\Form\Element;


use Zend\Form\Element\Select;

class Tags extends Select
{

    protected $attributes = array(
        'type' => 'select',
        'multiple' => true,
        'size' => '1'
    );

    /**
     * @param  null|int|string $name Optional name for the element
     * @param  array $options Optional options for the element
     */
    public function __construct($name = null, $options = [])
    {
        $options['value_options'] = [
            'specimen' => 'specimen',
            'primary' => 'primary',
            'label' => 'label',
            'habitat' => 'habitat',
            'carcass' => 'carcass',
            'skeletal' => 'skeletal',
            'microscope image' => 'microscope image',
            'drawing' => 'drawing',
            'egg' => 'egg',
            'larva' => 'larva',
            'pupa' => 'pupa',
            'juvenile' => 'juvenile',
            'nymph' => 'nymph',
            'subimago' => 'subimago',
            'immature' => 'immature',
            'mature' => 'mature',
            'adult' => 'adult',
            'tadpole' => 'tadpole',
            'dead' => 'dead',
            'alive' => 'alive',
            'embryo' => 'embryo',
            'subadult' => 'subadult',
            'pullus' => 'pullus',
            'gall' => 'gall',
            'fertile' => 'fertile',
            'sterile' => 'sterile',
            'seed' => 'seed',
            'spore' => 'spore',
            'fruit' => 'fruit',
            'sprout' => 'sprout',
            'bud' => 'bud',
            'flower' => 'flower',
            'shoot' => 'shoot',
            'root' => 'root',
            'leaf' => 'leaf',
            'stem' => 'stem',
            'trunk' => 'trunk',
            'inflorescence' => 'inflorescence',
            'cone' => 'cone',
            'sporangium' => 'sporangium',
        ];
        parent::__construct($name, $options);
    }

    public function setValue($value)
    {
        if ($value !== null && $value !== '' && $value !== []) {
            $valueOptions = $this->getValueOptions();
            $values = $value;
            if (is_string($values)) {
                $values = [$values];
            }
            $hasNew = false;
            foreach ($values as $v) {
                if (!in_array($v, $valueOptions)) {
                    $hasNew = true;
                    $valueOptions[$v] = $v;
                }
            }
            if ($hasNew) {
                $this->setValueOptions($valueOptions);
            }
        }
        parent::setValue($value);
        return $this;
    }


    /**
     * Provide default input rules for this element
     *
     * @return array
     */
    public function getInputSpecification()
    {
        $spec = array(
            'name' => $this->getName(),
            'required' => false,
        );

        return $spec;
    }

}