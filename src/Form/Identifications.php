<?php
namespace ImageApi\Form;


use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\Explode as ExplodeValidator;
use Zend\Validator\Regex;

class Identifications extends Fieldset implements InputFilterProviderInterface
{

    public function init()
    {
        parent::init();

        $this->add(array(
            'name' => 'taxonIds',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'taxonIds'
            ),
            'options' => array(
                'help' => 'Fill this in if you want to show the images on laji.fi species pages.',
                'label' => 'Taxon id',
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'taxonId',
                    'type' => 'text',
                )
            )
        ));

        $this->add(array(
            'name' => 'verbatim',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'verbatim'
            ),
            'options' => array(
                'count' => 0,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'verbatim',
                    'type' => 'hidden',
                )
            )
        ));
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $validator = new Regex([
            'pattern' => '/^MX\.[0-9]+$/',
        ]);
        $validator->setMessages([
            Regex::NOT_MATCH => 'Taxon id is not in correct form! Please only use the MX codes',
        ]);

        $validator = new ExplodeValidator(array(
            'validator' => $validator,
            'valueDelimiter' => null, // skip explode if only one value
        ));

        return array(
            'taxonIds' => array(
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'ImageApi\Filter\EmptyArray'
                    )
                ),
                'validators' => [
                    $validator
                ]
            ),
            'verbatim' => array(
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'ImageApi\Filter\EmptyArray'
                    )
                )
            ),
        );
    }
}