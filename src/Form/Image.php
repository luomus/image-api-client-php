<?php

namespace ImageApi\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class Imageis the form used for validating image file
 * @package ImagerServer\Form
 */
class Image extends Form implements InputFilterProviderInterface
{
    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        $this->setName('image');

        $this->add(array(
            'name' => 'image',
            'type' => 'file',
        ));
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'image' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Zend\Validator\File\Size',
                        'options' => array(
                            'max' => '350MB',
                            'messages' => array(
                                \Zend\Validator\File\Size::TOO_BIG => "Maximum allowed size for file is '%max%' but '%size%' detected."
                            )
                        )
                    ),
                    array(
                        'name' => 'Zend\Validator\File\IsImage',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\File\IsImage::FALSE_TYPE => "Only images please. Now '%type%' detected"
                            )
                        )
                    ),
                    /* Not working properly atm
                    array(
                        'name' => 'Zend\Validator\File\Count',
                        'options' => array(
                            'max' => 100,
                            'messages' => array(
                                \Zend\Validator\File\Count::TOO_MANY=> "Too many images, maximum '%max%' are allowed. Please use path option to upload your images or upload them in smaller sets."
                            )
                        )
                    )
                    */
                )
            )
        );
    }


}
