<?php

namespace ImageApi\Form;

use ImageApi\Model\PictureMeta as BindObject;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable;
use Zend\Validator\Date;

/**
 * Class PictureMeta is the form used for editing picture metadata
 * @package ImagerServer\Form
 */
class PictureMeta extends Form implements InputFilterProviderInterface
{
    const DEFAULT_LICENCE = 'MZ.intellectualRightsCC-BY-SA-4.0';
    const DEFAULT_RIGHTS_OWNER = 'Luomus';

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        $this->setName('picture-meta');
        $this->setObject(new BindObject());
        $this->setHydrator(new ArraySerializable());
        $this->setUseAsBaseFieldset(true);

        $this->add(array(
            'name' => 'documentIds',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'documentIds'
            ),
            'options' => array(
                'label' => 'Specimen id (full URI)',
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'documentId',
                    'type' => 'text',
                )
            )
        ));
        /*
        $this->add(array(
            'name' => 'documentIds',
            'type' => 'ImageApi\Form\Element\Ids',
            'attributes' => array(
                'id' => 'documentIds'
            ),
            'options' => array(
                'label' => 'Document ids (full uris separated with comma)',
            )
        ));
        */
        $this->add(array(
            'name' => 'tags',
            'type' => 'ImageApi\Form\Element\Tags',
            'attributes' => array(
                'id' => 'tags'
            ),
            'options' => array(
                'label' => 'Tags',
            )
        ));

        $this->add(array(
            'name' => 'capturers',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'capturers'
            ),
            'options' => array(
                'label' => 'Capturers',
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'capturer',
                    'type' => 'text',
                    'options' => array(
                        'template' => 'kotka/partial/element/add-me',
                        'skip-group' => true,
                        'add-me' => array(
                            'name' => 'this',
                        )
                    ),
                )
            )
        ));

        $this->add(array(
            'name' => 'rightsOwner',
            'type' => 'text',
            'options' => array(
                'label' => 'Holder of intellectual copyright',
                'template' => 'kotka/partial/element/add-me',
                'add-me' => array(
                    'name' => '.rights-owner',
                ),
            ),
            'attributes' => array(
                'class' => 'rights-owner',
                'value' => self::DEFAULT_RIGHTS_OWNER,
                'placeholder' => self::DEFAULT_RIGHTS_OWNER,
            )
        ));

        $this->add(array(
            'name' => 'license',
            'type' => 'DatabaseSelect',
            'options' => array(
                'label' => 'Intellectual copyright license',
                'field' => 'MZ.intellectualRights',
            ),
        ));
        $this->get('license')->setValue(self::DEFAULT_LICENCE);

        $this->add(array(
            'name' => 'luomusRights',
            'type' => 'DatabaseSelect',
            'options' => array(
                'label' => 'Intellectual rights given to ' . self::DEFAULT_RIGHTS_OWNER,
                'empty_option' => 'Select',
                'field' => 'MM.LuomusIntellectualRights'
            ),
        ));

        $this->add(array(
            'name' => 'captureDateTime',
            'type' => 'text',
            'options' => array(
                'label' => 'Date/time when the image was taken',
                'template' => 'kotka/partial/element/datetime-picker',
            ),
            'attributes' => array(
                'class' => 'form-control image-datetime-picker',
            )
        ));

        $this->add(array(
            'name' => 'identifications',
            'type' => 'ImageApi\Form\Identifications'
        ));

        $this->add(array(
            'name' => 'taxonDescriptionCaptionEn',
            'type' => 'text',
            'options' => array(
                'label' => 'Caption (en)',
            ),
        ));
        $this->add(array(
            'name' => 'taxonDescriptionCaptionFi',
            'type' => 'text',
            'options' => array(
                'label' => 'Caption (fi)',
            ),
        ));
        $this->add(array(
            'name' => 'taxonDescriptionCaptionSv',
            'type' => 'text',
            'options' => array(
                'label' => 'Caption (sv)',
            ),
        ));
    }

    public function addAdvancedFields()
    {
        $this->add(array(
            'name' => 'wgs84Coordinates',
            'type' => 'ImageApi\Form\WGS84Coordinates'
        ));
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'capturers' => array(
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'ImageApi\Filter\EmptyArray'
                    )
                )
            ),
            'rightsOwner' => array(
                'required' => false, //This is auto completed with default value if empty (in ImageService::setDefaultMeta())
            ),
            'luomusRights' => array(
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'Null'
                    )
                )
            ),
            'license' => array(
                'required' => true
            ),
            'captureDateTime' => array(
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'Null'
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'date',
                        'options' => array(
                            'format' => BindObject::DATE_TIME_FORMAT,
                            'messages' => array(
                                Date::INVALID => 'Date not in valid format',
                                Date::INVALID_DATE => 'Invalid date given'
                            )
                        )
                    )
                )
            ),
        );
    }


}
