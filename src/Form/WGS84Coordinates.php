<?php
namespace ImageApi\Form;

use Zend\Form\Fieldset;

class WGS84Coordinates extends Fieldset
{

    public function init()
    {
        parent::init();

        $this->add(array(
            'name' => 'lat',
            'type' => 'text',
            'attributes' => array(
                'id' => 'lat'
            ),
            'options' => array(
                'label' => 'Latitude',
            )
        ));

        $this->add(array(
            'name' => 'lon',
            'type' => 'text',
            'attributes' => array(
                'id' => 'lon'
            ),
            'options' => array(
                'label' => 'Longitude',
            )
        ));


    }
}