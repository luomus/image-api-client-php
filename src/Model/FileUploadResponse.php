<?php
namespace ImageApi\Model;

use Zend\Stdlib\ArraySerializableInterface;

class FileUploadResponse implements ArraySerializableInterface
{

    private $name;
    private $filename;
    private $id;
    private $expires;

    /**
     * @return string
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * @param string $expires
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * Exchange internal values from provided array
     *
     * @param  array $array
     * @return void
     */
    public function exchangeArray(array $array)
    {
        if (is_numeric(key($array))) {
            $array = array_pop($array);
        }
        $this->id = (isset($array['id'])) ? $array['id'] : null;
        $this->name = (isset($array['name'])) ? $array['name'] : null;
        $this->filename = (isset($array['filename'])) ? $array['filename'] : null;
        $this->expires = (isset($array['expires'])) ? $array['expires'] : null;
    }

    /**
     * Return an array representation of the object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}