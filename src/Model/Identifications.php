<?php
namespace ImageApi\Model;

use Zend\Stdlib\ArraySerializableInterface;

class Identifications implements ArraySerializableInterface
{
    private $taxonIds = array();
    private $verbatim = array();

    /**
     * @return array
     */
    public function getTaxonIds()
    {
        return $this->taxonIds;
    }

    /**
     * @param array $taxonIds
     */
    public function setTaxonIds($taxonIds)
    {
        $this->taxonIds = $taxonIds;
    }

    /**
     * @return array
     */
    public function getVerbatim()
    {
        return $this->verbatim;
    }

    /**
     * @param array $verbatim
     */
    public function setVerbatim($verbatim)
    {
        $this->verbatim = $verbatim;
    }


    /**
     * Exchange internal values from provided array
     *
     * @param  array $array
     * @return void
     */
    public function exchangeArray(array $array)
    {
        if (isset($array['taxonIds'])) {
            if (is_string($array['taxonIds'])) {
                $array['taxonIds'] = [$array['taxonIds']];
            }
            if (is_array($array['taxonIds'])) {
                $this->taxonIds = $array['taxonIds'];
            }
        }
        if (isset($array['verbatim'])) {
            if (is_string($array['verbatim'])) {
                $array['verbatim'] = [$array['verbatim']];
            }
            if (is_array($array['verbatim'])) {
                $this->verbatim = $array['verbatim'];
            }
        }
    }

    /**
     * Return an array representation of the object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}