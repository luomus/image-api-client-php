<?php

namespace ImageApi\Model;


use Zend\Stdlib\ArraySerializableInterface;

class Media implements ArraySerializableInterface
{
    private $id;
    private $secretKey;
    /** @var Urls */
    private $urls;
    /** @var PictureMeta */
    private $meta;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return PictureMeta
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param PictureMeta $meta
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;
    }

    /**
     * @return Urls
     */
    public function getUrls()
    {
        return $this->urls;
    }

    /**
     * @param Urls $urls
     */
    public function setUrl($urls)
    {
        $this->urls = $urls;
    }

    /**
     * @return string
     */
    public function getSecretKey()
    {
        return $this->secretKey;
    }

    /**
     * @param string $secretKey
     */
    public function setSecretKey($secretKey)
    {
        $this->secretKey = $secretKey;
    }

    public function exchangeArray(array $array, $convertFromApi = false)
    {
        $this->id = (isset($array['id'])) ? $array['id'] : null;
        $this->secretKey = (isset($array['secretKey'])) ? $array['secretKey'] : null;
        $this->urls = new Urls();
        $this->meta = new PictureMeta();
        if (isset($array['urls'])) {
            $this->urls->exchangeArray($array['urls'], $convertFromApi);
        }
        if (isset($array['meta'])) {
            $this->meta->exchangeArray($array['meta'], $convertFromApi);
        }
    }

    public function getArrayCopy($convertForApi = false)
    {
        $results = get_object_vars($this);
        if (isset($results['urls']) && $results['urls'] instanceof ArraySerializableInterface) {
            $results['urls'] = $results['urls']->getArrayCopy($convertForApi);
        }
        if (isset($results['meta']) && $results['meta'] instanceof ArraySerializableInterface) {
            $results['meta'] = $results['meta']->getArrayCopy($convertForApi);
        }
        return $results;
    }
}