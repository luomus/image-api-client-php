<?php
namespace ImageApi\Model;

use Zend\Stdlib\ArraySerializableInterface;

class NewMedia implements ArraySerializableInterface
{
    /** @var  PictureMeta */
    private $meta;
    private $tempFileId;

    /**
     * @return PictureMeta
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param PictureMeta $meta
     */
    public function setMeta(PictureMeta $meta)
    {
        $this->meta = $meta;
    }

    /**
     * @return string
     */
    public function getTempFileId()
    {
        return $this->tempFileId;
    }

    /**
     * @param string $tempFileId
     */
    public function setTempFileId($tempFileId)
    {
        $this->tempFileId = $tempFileId;
    }


    /**
     * Exchange internal values from provided array
     *
     * @param  array $array
     * @return void
     */
    public function exchangeArray(array $array)
    {
        $this->tempFileId = (isset($array['tempFileId'])) ? $array['tempFileId'] : null;
        $this->meta = new PictureMeta();
        if (isset($array['meta'])) {
            $this->meta->exchangeArray($array['meta']);
        }
    }

    /**
     * Return an array representation of the object
     *
     * @param boolean $convertForApi converts values for api
     *
     * @return array
     */
    public function getArrayCopy($convertForApi = false)
    {
        $results = get_object_vars($this);
        if (isset($results['meta']) && $results['meta'] instanceof ArraySerializableInterface) {
            $results['meta'] = $results['meta']->getArrayCopy($convertForApi);
        }

        return $results;
    }
}