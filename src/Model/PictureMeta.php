<?php

namespace ImageApi\Model;

use DateTime;
use Zend\Stdlib\ArraySerializableInterface;

class PictureMeta implements ArraySerializableInterface
{

    const DATE_TIME_FORMAT = 'Y-m-d H:i';
    const DATE_TIME_FORMAT_JS = 'YYYY-MM-DD HH:mm';

    private $originalFilename;
    private $capturers = array();
    private $rightsOwner;
    private $license;
    private $luomusRights;
    /** @var Identifications */
    private $identifications;
    private $captureDateTime;
    private $tags = array();
    /** @var WGS84Coordinates */
    private $wgs84Coordinates;
    private $documentIds;
    private $caption;
    private $taxonDescriptionCaptionEn;
    private $taxonDescriptionCaptionFi;
    private $taxonDescriptionCaptionSv;
    private $uploadedBy;
    private $sourceSystem;
    private $secret = false;
    private $allowLoggedOut = false;

    /**
     * @return boolean
     */
    public function isAllowLoggedOut()
    {
        return $this->allowLoggedOut;
    }

    /**
     * @param boolean $allowLoggedOut
     */
    public function setAllowLoggedOut($allowLoggedOut)
    {
        $this->allowLoggedOut = $allowLoggedOut;
    }

    /**
     * @return string
     */
    public function getCaptureDateTime()
    {
        return $this->captureDateTime;
    }

    /**
     * @param string $captureDateTime
     */
    public function setCaptureDateTime($captureDateTime)
    {
        $this->captureDateTime = $captureDateTime;
    }

    /**
     * @return array
     */
    public function getCapturers()
    {
        return $this->capturers;
    }

    /**
     * @param array $capturers
     */
    public function setCapturers($capturers)
    {
        $this->capturers = $capturers;
    }

    /**
     * @return Identifications
     */
    public function getIdentifications()
    {
        return $this->identifications;
    }

    /**
     * @param Identifications $identifications
     */
    public function setIdentifications(Identifications $identifications)
    {
        $this->identifications = $identifications;
    }

    /**
     * @return array
     */
    public function getDocumentIds()
    {
        return $this->documentIds;
    }

    /**
     * @param array $documentIds
     */
    public function setDocumentIds(array $documentIds = [])
    {
        $this->documentIds = $documentIds;
    }

    /**
     * @return string
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * @param string $license
     */
    public function setLicense($license)
    {
        $this->license = $license;
    }

    /**
     * @return string
     */
    public function getLuomusRights()
    {
        return $this->luomusRights;
    }

    /**
     * @param string $luomusRights
     */
    public function setLuomusRights($luomusRights)
    {
        $this->luomusRights = $luomusRights;
    }

    /**
     * @return string
     */
    public function getOriginalFilename()
    {
        return $this->originalFilename;
    }

    /**
     * @param string $originalFilename
     */
    public function setOriginalFilename($originalFilename)
    {
        $this->originalFilename = $originalFilename;
    }

    /**
     * @return string
     */
    public function getRightsOwner()
    {
        return $this->rightsOwner;
    }

    /**
     * @param string $rightsOwner
     */
    public function setRightsOwner($rightsOwner)
    {
        $this->rightsOwner = $rightsOwner;
    }

    /**
     * @return boolean
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param boolean $secret
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }

    /**
     * @return string
     */
    public function getSourceSystem()
    {
        return $this->sourceSystem;
    }

    /**
     * @param string $sourceSystem
     */
    public function setSourceSystem($sourceSystem)
    {
        $this->sourceSystem = $sourceSystem;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return string
     */
    public function getUploadedBy()
    {
        return $this->uploadedBy;
    }

    /**
     * @param string $uploadedBy
     */
    public function setUploadedBy($uploadedBy)
    {
        $this->uploadedBy = $uploadedBy;
    }

    /**
     * @return WGS84Coordinates
     */
    public function getWgs84Coordinates()
    {
        return $this->wgs84Coordinates;
    }

    /**
     * @param WGS84Coordinates $wgs84Coordinates
     */
    public function setWgs84Coordinates(WGS84Coordinates $wgs84Coordinates)
    {
        $this->wgs84Coordinates = $wgs84Coordinates;
    }

    /**
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @param string $caption
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    /**
     * @return string
     */
    public function getTaxonDescriptionCaptionEn()
    {
        return $this->taxonDescriptionCaptionEn;
    }

    /**
     * @param string $taxonDescriptionCaptionEn
     */
    public function setTaxonDescriptionCaptionEn($taxonDescriptionCaptionEn)
    {
        $this->taxonDescriptionCaptionEn = $taxonDescriptionCaptionEn;
    }

    /**
     * @return string
     */
    public function getTaxonDescriptionCaptionFi()
    {
        return $this->taxonDescriptionCaptionFi;
    }

    /**
     * @param string $taxonDescriptionCaptionFi
     */
    public function setTaxonDescriptionCaptionFi($taxonDescriptionCaptionFi)
    {
        $this->taxonDescriptionCaptionFi = $taxonDescriptionCaptionFi;
    }

    /**
     * @return string
     */
    public function getTaxonDescriptionCaptionSv()
    {
        return $this->taxonDescriptionCaptionSv;
    }

    /**
     * @param string $taxonDescriptionCaptionSv
     */
    public function setTaxonDescriptionCaptionSv($taxonDescriptionCaptionSv)
    {
        $this->taxonDescriptionCaptionSv = $taxonDescriptionCaptionSv;
    }

    /**
     * Exchange internal values from provided array
     *
     * @param  array $array
     * @param  bool $convertFromApi
     * @return void
     */
    public function exchangeArray(array $array, $convertFromApi = false)
    {
        if (isset($array['originalFilename'])) {
            $this->originalFilename = $array['originalFilename'];
        }
        if (isset($array['rightsOwner'])) {
            $this->rightsOwner = $array['rightsOwner'];
        }
        if (isset($array['luomusRights'])) {
            $this->luomusRights = $array['luomusRights'];
        }
        if (isset($array['license'])) {
            $this->license = $array['license'];
        }
        if (isset($array['documentIds'])) {
            $this->documentIds = $array['documentIds'];
        }
        if (isset($array['uploadedBy'])) {
            $this->uploadedBy = $array['uploadedBy'];
        }
        if (isset($array['sourceSystem'])) {
            $this->sourceSystem = $array['sourceSystem'];
        }
        if (isset($array['secret'])) {
            $this->secret = $array['secret'];
        }
        if (isset($array['capturers'])) {
            $this->capturers = $array['capturers'];
        }
        if (isset($array['tags'])) {
            $this->tags = $array['tags'];
        } else {
        /**
         * required as select-element used for this results in unset tags when empty, 
         * resulting in tags being impossible to remove
         */
            $this->tags = [];
        }
        if (isset($array['caption'])) {
            $this->caption = $array['caption'];
        }
        if (isset($array['taxonDescriptionCaptionEn'])) {
            $this->taxonDescriptionCaptionEn = $array['taxonDescriptionCaptionEn'];
        }
        if (isset($array['taxonDescriptionCaptionFi'])) {
            $this->taxonDescriptionCaptionFi = $array['taxonDescriptionCaptionFi'];
        }
        if (isset($array['taxonDescriptionCaptionSv'])) {
            $this->taxonDescriptionCaptionSv = $array['taxonDescriptionCaptionSv'];
        }
        if (isset($array['taxonDescriptionCaption'])) {
            if (isset($array['taxonDescriptionCaption']['fi'])) {
                $this->taxonDescriptionCaptionFi = $array['taxonDescriptionCaption']['fi'];
            }
            if (isset($array['taxonDescriptionCaption']['en'])) {
                $this->taxonDescriptionCaptionEn = $array['taxonDescriptionCaption']['en'];
            }
            if (isset($array['taxonDescriptionCaption']['sv'])) {
                $this->taxonDescriptionCaptionSv = $array['taxonDescriptionCaption']['sv'];
            }
        }

        if ($this->identifications === null) {
            $this->identifications = new Identifications();
        }
        if ($this->wgs84Coordinates === null) {
            $this->wgs84Coordinates = new WGS84Coordinates();
        }
        if (isset($array['identifications'])) {
            $this->identifications->exchangeArray($array['identifications']);
        }
        if (isset($array['wgs84Coordinates'])) {
            $this->wgs84Coordinates->exchangeArray($array['wgs84Coordinates']);
        }
        if (isset($array['captureDateTime'])) {
            if ($convertFromApi) {
                try {
                    $date = new DateTime();
                    $date->setTimestamp($array['captureDateTime']);
                    $this->captureDateTime = $date->format(self::DATE_TIME_FORMAT);
                } catch (\Exception $e) {
                }
            } else {
                $this->captureDateTime = $array['captureDateTime'];
            }

        }

    }

    /**
     * Return an array representation of the object
     *
     * @param boolean $convertForApi converts values for api
     *
     * @return array
     */
    public function getArrayCopy($convertForApi = false)
    {
        $results = get_object_vars($this);
        if (isset($results['identifications']) && $results['identifications'] instanceof ArraySerializableInterface) {
            $results['identifications'] = $results['identifications']->getArrayCopy();
        }
        if (isset($results['wgs84Coordinates']) && $results['wgs84Coordinates'] instanceof ArraySerializableInterface) {
            $results['wgs84Coordinates'] = $results['wgs84Coordinates']->getArrayCopy();
        }
        if ($convertForApi) {
            $results['taxonDescriptionCaption'] = [
                'en' => isset($results['taxonDescriptionCaptionEn']) ? $results['taxonDescriptionCaptionEn'] : '',
                'fi' => isset($results['taxonDescriptionCaptionFi']) ? $results['taxonDescriptionCaptionFi'] : '',
                'sv' => isset($results['taxonDescriptionCaptionSv']) ? $results['taxonDescriptionCaptionSv'] : ''
            ];
            unset($results['taxonDescriptionCaptionEn']);
            unset($results['taxonDescriptionCaptionFi']);
            unset($results['taxonDescriptionCaptionSv']);
            if (isset($results['captureDateTime'])) {
                try {
                    $date = DateTime::createFromFormat(self::DATE_TIME_FORMAT, $results['captureDateTime']);
                    if ($date !== false) {
                        $results['captureDateTime'] = (int)$date->getTimestamp();
                    }
                } catch (\Exception $e) {
                }
            }
        }
        if (isset($results['allowLoggedOut'])) {
            unset($results['allowLoggedOut']);
        }
        return $results;
    }
}