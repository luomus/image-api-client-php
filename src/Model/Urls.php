<?php
namespace ImageApi\Model;

use Zend\Stdlib\ArraySerializableInterface;

class Urls implements ArraySerializableInterface
{
    private $original;
    private $full;
    private $large;
    private $square;
    private $thumbnail;

    /**
     * @return string
     */
    public function getOriginal()
    {
        return $this->original;
    }

    /**
     * @param string $original
     */
    public function setOriginal($original)
    {
        $this->original = $original;
    }

    /**
     * @return string
     */
    public function getFull()
    {
        return $this->full;
    }

    /**
     * @param string $full
     */
    public function setFull($full)
    {
        $this->full = $full;
    }

    /**
     * @return string
     */
    public function getLarge()
    {
        return $this->large;
    }

    /**
     * @param string $large
     */
    public function setLarge($large)
    {
        $this->large = $large;
    }

    /**
     * @return string
     */
    public function getSquare()
    {
        return $this->square;
    }

    /**
     * @param string $square
     */
    public function setSquare($square)
    {
        $this->square = $square;
    }

    /**
     * @return string
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param string $thumbnail
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * Exchange internal values from provided array
     *
     * @param  array $array
     * @return void
     */
    public function exchangeArray(array $array)
    {
        $this->original = (isset($array['original'])) ? $array['original'] : null;
        $this->full = (isset($array['full'])) ? $array['full'] : null;
        $this->large = (isset($array['large'])) ? $array['large'] : null;
        $this->square = (isset($array['square'])) ? $array['square'] : null;
        $this->thumbnail = (isset($array['thumbnail'])) ? $array['thumbnail'] : null;
    }

    /**
     * Return an array representation of the object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}