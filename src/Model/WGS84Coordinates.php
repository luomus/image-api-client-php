<?php

namespace ImageApi\Model;

use Zend\Stdlib\ArraySerializableInterface;

class WGS84Coordinates implements ArraySerializableInterface
{
    private $lat;
    private $lon;

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * @param float $lon
     */
    public function setLon($lon)
    {
        $this->lon = $lon;
    }


    /**
     * Exchange internal values from provided array
     *
     * @param  array $array
     * @return void
     */
    public function exchangeArray(array $array)
    {
        $this->lat = (isset($array['lat']) && !empty($array['lat'])) ? (float)$array['lat'] : null;
        $this->lon = (isset($array['lon']) && !empty($array['lat'])) ? (float)$array['lon'] : null;
    }

    /**
     * Return an array representation of the object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}