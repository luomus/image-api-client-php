<?php
namespace ImageApi\Options;

use Zend\Stdlib\AbstractOptions;

class ImageServerConfiguration extends AbstractOptions
{
    private $serverLocation;
    private $system;
    private $username;
    private $password;
    private $imageParam = 'image';
    private $cert;

    public function getServerLocation()
    {
        return $this->serverLocation;
    }

    public function setServerLocation($location)
    {
        $this->serverLocation = $location;
    }

    public function getSystem()
    {
        return $this->system;
    }

    public function setSystem($system)
    {
        $this->system = $system;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getImageParam()
    {
        return $this->imageParam;
    }

    public function setImageParam($imageParam)
    {
        $this->imageParam = $imageParam;
    }

    public function getCert()
    {
        return $this->cert;
    }

    public function setCert($cert)
    {
        $this->cert = $cert;
    }

} 