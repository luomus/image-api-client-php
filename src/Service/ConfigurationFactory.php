<?php

namespace ImageApi\Service;

use ImageApi\Options\ImageServerConfiguration;
use RuntimeException;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ConfigurationFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $options = $this->getOptions($serviceLocator);
        $config = new ImageServerConfiguration($options);

        return $config;
    }

    /**
     * @param  ServiceLocatorInterface $serviceLocator
     * @return mixed
     * @throws RuntimeException
     */
    public function getOptions(ServiceLocatorInterface $serviceLocator)
    {
        $options = $serviceLocator->get('Config');
        $options = isset($options['imageserver']) ? $options['imageserver'] : null;

        if (null === $options) {
            throw new RuntimeException(
                'Configuration for image service could not be found in "imageserver".'
            );
        }

        return $options;
    }
}
