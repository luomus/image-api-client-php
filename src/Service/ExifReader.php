<?php

namespace ImageApi\Service;


class ExifReader
{

    private $file;
    private $hasExif = false;
    private $exif = [];

    public function __construct($file = null)
    {
        if ($file !== null) {
            $this->setFile($file);
        }
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        if (!file_exists($file) || !is_readable($file)) {
            return;
        }

        /**
         * check if file is png, and mark exif as nonexistant if yes,
         * as exif_read_data causes an error for png:s as they have none
         */
        if (exif_imagetype($file) === 3) {
          $this->hasExif = false;
          return;
        }

        $this->file = $file;
        //supressing erros as some jpg:s don't have understandabel exif:s
        $exif = @exif_read_data($file, null, true);
        if ($exif === false) {
            $this->hasExif = false;
            return;
        }
        $this->hasExif = true;
        $this->exif = $exif;
    }

    /**
     * @return boolean
     */
    public function hasExif()
    {
        return $this->hasExif;
    }

    public function has($field, $section = null)
    {
        if ($section === null && isset($this->exif[$field])) {
            return true;
        } else if (isset($this->exif[$section]) && isset($this->exif[$section][$field])) {
            return true;
        }
        return false;
    }

    public function get($field, $section = null, $default = null)
    {
        if (!$this->has($field, $section)) {
            return $default;
        }
        if ($section === null) {
            if (isset($this->exif[$field])) {
                return $this->exif[$field];
            }
        }
        return $this->exif[$section][$field];
    }

    public function getDateTimeOriginal($default = null, $asDateTime = true)
    {
        $dateTime = $this->get('DateTimeOriginal', 'EXIF', $default);
        if ($dateTime === $default) {
            return $dateTime;
        }
        if ($asDateTime) {
            $dateTime = new \DateTime($dateTime);
        }
        return $dateTime;
    }

} 