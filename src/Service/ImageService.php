<?php

namespace ImageApi\Service;


use ImageApi\Form\Element\Tags;
use ImageApi\Model\FileUploadResponse;
use ImageApi\Model\Identifications;
use ImageApi\Model\Media;
use ImageApi\Model\NewMedia;
use ImageApi\Model\PictureMeta;
use ImageApi\Model\WGS84Coordinates;
use ImageApi\Options\ImageServerConfiguration;
use Common\Service\IdService;
use Zend\EventManager\EventManager;
use Zend\Http\Client;
use Zend\Http\Headers;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Session\Container;
use Zend\Stdlib\ArraySerializableInterface;

class ImageService
{

    const DATA_CONTANER = 'data';
    const NAMESPACE_DATA = '_ns_image_prefix_data_';

    const PATH_FILE_UPLOAD = 'api/fileUpload';
    const PATH_IMAGES = 'api/images';

    private $conf;
    /** @var Container */
    private $container;
    private $response;
    private $domainPrefixes;

    private $searchWhitelist = [
        'documentIds',
        'capturers',
        'rightsOwner',
        'identifications.taxonIds'
    ];

    public function __construct(ImageServerConfiguration $configuration, $domainPrefix = null)
    {
        $this->conf = $configuration;
        $this->container = new Container('imageService');
        $this->domainPrefixes = $domainPrefix;
    }

    /**
     * @return Response
     */
    public function getServerResponse()
    {
        return $this->response;
    }

    public function getResponseMessage()
    {
        $json = $this->getServerResponse()->getBody();
        $message = $json;
        try {
            $data = json_decode($json, true);
            if (is_array($data)) {
                if (isset($data['message'])) {
                    $message = $data['message'];
                } elseif ($data['code']) {
                    $message = $data['code'];
                }

            }
        } catch (\Exception $e) {
        }

        return $message;
    }

    public function search(array $params, $asArray = false)
    {
        if (empty($params)) {
            return [];
        }
        $parsed = [];
        foreach ($params as $key => $value) {
            $key = str_replace(['_', '/'], '.', $key);
            $parsed[$key] = $value;
        }
        $parsed = array_intersect_key($parsed, array_flip($this->searchWhitelist));
        $server = $this->conf->getServerLocation();
        $imageUrl = $server . self::PATH_IMAGES;
        $client = $this->getHttpClient();
        $client->setUri($imageUrl);
        $client->setMethod(Request::METHOD_GET);
        $client->setParameterGet($parsed);
        return $this->processResponse($client->send(), $asArray);
    }

    public function getImages($documentId, $asArray = false)
    {
        if ($this->triggerEvent('show', ['documentId' => $documentId]) === false) {
            return false;
        }
        return $this->search(['documentIds' => $documentId], $asArray);
    }

    public function getImage($id, $asArray = false)
    {
        $server = $this->conf->getServerLocation();
        $imageUrl = $server . self::PATH_IMAGES . '/' . $id;
        $client = $this->getHttpClient();
        $client->setUri($imageUrl);
        $client->setMethod(Request::METHOD_GET);
        $this->response = $client->send();
        $media = $this->processResponse($this->response, $asArray, true);
        if ($media instanceof Media) {
            if ($this->triggerEvent('show', ['meta' => $media->getMeta()]) === false) {
                return false;
            }
        } else if (is_array($media)) {
            if ($this->triggerEvent('show', ['meta' => $media['meta']]) === false) {
                return false;
            }
        }
        return $media;
    }

    private function processResponse(Response $response, $asArray, $single = false)
    {
        if (!$response->isOk()) {
            return false;
        }
        $data = json_decode($response->getBody(), true);
        if ($single) {
            $media = new Media();
            $media->exchangeArray($data, true);
            if ($asArray) {
                return $media->getArrayCopy();
            }
            return $media;
        }
        foreach ($data as $key => $image) {
            $media = new Media();
            $media->exchangeArray($image, true);
            if ($asArray) {
                $data[$key] = $media->getArrayCopy();
            } else {
                $data[$key] = $media;
            }
        }
        return $data;
    }

    public function removeImage($id, $triggerEvent = true)
    {
        if ($triggerEvent && $this->triggerEvent('remove', ['meta' => $this->getImage($id)]) === false) {
            return false;
        }
        $server = $this->conf->getServerLocation();
        $imageUrl = $server . self::PATH_IMAGES . '/' . $id;
        $client = $this->getHttpClient();
        $this->addJsonHeaders($client);
        $client->setUri($imageUrl);
        $client->setMethod(Request::METHOD_DELETE);
        $this->response = $client->send();
        if ($this->response->getStatusCode() == 204) {
            return true;
        }
        return false;
    }

    public function updateMeta($id, PictureMeta $meta)
    {
        if ($this->triggerEvent('update', ['meta' => $meta]) === false) {
            return false;
        }
        $server = $this->conf->getServerLocation();
        $url = $server . self::PATH_IMAGES . '/' . $id;
        $client = $this->getHttpClient();
        $this->addJsonHeaders($client);
        $client->setUri($url);
        $client->setMethod(Request::METHOD_PUT);
        $json = json_encode($meta->getArrayCopy(true));
        $client->setRawBody($json);
        $this->response = $client->send();
        if (!$this->response->isSuccess()) {
            return false;
        }
        return $this->response->getBody();
    }

    public function uploadPicture($file, PictureMeta $meta, $format = null, $setDefaults = true)
    {
        $this->setDefaultMeta($meta, $file, $setDefaults);
        if ($this->uploadEvent($meta) === false) {
            return false;
        }
        try {
            $this->processFormattedFilename($file, $meta, $format);
            $server = $this->conf->getServerLocation();
            $fileUploadUrl = $server . self::PATH_FILE_UPLOAD;
            $metaUploadUrl = $server . self::PATH_IMAGES;
            $client = $this->getHttpClient();

            $client->setUri($fileUploadUrl);
            $client->setFileUpload($file, 'file');
            $client->setMethod(Request::METHOD_POST);
            $this->response = $client->send();
            if (!$this->response->isSuccess()) {
                return false;
            }
            $newMedia = new NewMedia();
            $fileResponse = new FileUploadResponse();
            $fileResponse->exchangeArray(json_decode($this->response->getBody(), true));
            $newMedia->setTempFileId($fileResponse->getId());
            $newMedia->setMeta($meta);
            $this->addJsonHeaders($client);
            $client->setUri($metaUploadUrl);
            $json = json_encode([$newMedia->getArrayCopy(true)]);
            $client->setRawBody($json);
            $this->response = $client->send();
            if (!$this->response->isSuccess()) {
                return false;
            }
            $data = json_decode($this->response->getBody(), true);
        } catch (\Exception $e) {
            $this->response = new Response();
            $this->response->setContent("Failed to connect: " . $e->getMessage());
            $this->response->setStatusCode(500);
            return false;
        }
        return $data[0]['id'];
    }

    public function uploadEvent(PictureMeta $meta)
    {
        if ($this->triggerEvent('upload', ['meta' => $meta]) === false) {
            return false;
        }
        return true;
    }

    private function triggerEvent($event, array $args)
    {
        $eventManager = new EventManager('image');
        $result = $eventManager->trigger($event, null, $args);
        if ($result->stopped()) {
            $this->response = new Response();
            $response = $result->first();
            $code = 400;
            if (is_array($response)) {
                if (isset($response['code'])) {
                    $code = $response['code'];
                }
                if (isset($response['status'])) {
                    $response = $response['status'];
                } else {
                    $response = 'Progress was halted!';
                }
            }
            $this->response->setContent($response);
            $this->response->setStatusCode($code);
            return false;
        }
        return true;
    }

    public function setDefaultData(array $data)
    {
        if (isset($data['captureDateTime'])) {
            unset($data['captureDateTime']);
        }
        $this->container->offsetSet(self::DATA_CONTANER, $data);
    }

    public function setDefaultMeta(PictureMeta $meta, $file, $setDefaults)
    {
        $container = $this->container;
        if ($meta->getWgs84Coordinates() === null) {
            $meta->setWgs84Coordinates(new WGS84Coordinates());
        }
        if ($meta->getIdentifications() === null) {
            $meta->setIdentifications(new Identifications());
        }
        if (empty($meta->getDocumentIds()[0])) {
            if (!empty($meta->getIdentifications()->getTaxonIds()[0])) {
                $meta->setDocumentIds($meta->getIdentifications()->getTaxonIds());
            } else {
                $meta->setDocumentIds(['none']);
            }
        }
        if ($setDefaults) {
            if ($container !== null && $container->offsetExists(self::DATA_CONTANER)) {
                $data = $container->offsetGet(self::DATA_CONTANER);
                foreach ($data as $key => $value) {
                    $setMethod = 'set' . ucfirst($key);
                    $getMethod = 'get' . ucfirst($key);
                    if (method_exists($meta, $setMethod)) {
                        $objectValue = $meta->$getMethod();
                        if (!empty($objectValue)) {
                            continue;
                        }
                        if (is_array($value)) {
                            if ($objectValue instanceof ArraySerializableInterface) {
                                $objectValue->exchangeArray($value);
                                continue;
                            }
                        }
                        $meta->$setMethod($value);
                    }
                }
            }
            if ($meta->getCaptureDateTime() === null) {
                $exifReader = new ExifReader($file);
                $datetime = $exifReader->getDateTimeOriginal(false);
                if ($datetime instanceof \DateTime && $datetime > new \DateTime("1900-01-01") && $datetime < new \DateTime()) {
                    $meta->setCaptureDateTime($datetime->format(PictureMeta::DATE_TIME_FORMAT));
                }
            }
        }
        if ($meta->getRightsOwner() === null || $meta->getRightsOwner() === '') {
            $meta->setRightsOwner(\ImageApi\Form\PictureMeta::DEFAULT_RIGHTS_OWNER);
        }
        if ($meta->getLicense() === null || $meta->getLicense() === '') {
            $meta->setLicense(\ImageApi\Form\PictureMeta::DEFAULT_LICENCE);
        }
    }

    public function convertMediaArrayToOldApi($data, $qname = null, $licenses = [], $skipSecret = true)
    {
        $images = [];
        $imgCnt = 0;
        if (is_array($data)) {
            $imgCnt = count($data);
            foreach ($data as $image) {
                /** @var Media $image */
                if ($skipSecret && $image->getMeta()->getSecret()) {
                    $imgCnt--;
                    continue;
                }
                $license = $image->getMeta()->getLicense();
                if (isset($licenses[$license])) {
                    $license = $licenses[$license];
                }
                $img = [
                    'id' => $image->getId(),
                    'filename' => $image->getMeta()->getOriginalFilename(),
                    'web_url' => $image->getUrls()->getOriginal() ?: $image->getUrls()->getFull(),
                    'full' => $image->getUrls()->getFull(),
                    'thumbnail' => $image->getUrls()->getThumbnail(),
                    'square_thumbnail' => $image->getUrls()->getSquare(),
                    'medium' => $image->getUrls()->getLarge(),
                    'large' => $image->getUrls()->getLarge(),
                    'license' => $license,
                    'tags' => $image->getMeta()->getTags(),
                    'licenseID' => $image->getMeta()->getLicense(),
                    'copyright_owner' => $image->getMeta()->getRightsOwner(),
                    'publication_terms' => '',
                    'photographer_name' => $image->getMeta()->getCapturers(),
                    'qnames' => [$qname]
                ];
                if ($image->getMeta()->getSecret()) {
                    $img['web_url'] .= '?secret=' . $image->getSecretKey() . '&format=.jpg';
                    $img['full'] .= '?secret=' . $image->getSecretKey() . '&format=.jpg';
                    $img['thumbnail'] .= '?secret=' . $image->getSecretKey() . '&format=.jpg';
                    $img['square_thumbnail'] .= '?secret=' . $image->getSecretKey() . '&format=.jpg';
                    $img['medium'] .= '?secret=' . $image->getSecretKey() . '&format=.jpg';
                    $img['large'] .= '?secret=' . $image->getSecretKey() . '&format=.jpg';
                }

                $images[] = $img;
            }
        }

        $result = [
            'request_qname' => $qname,
            'num_images' => $imgCnt,
            'images' => $images
        ];

        return [$result];
    }

    private function processFormattedFilename($file, PictureMeta $meta, $format = null)
    {
        if ($format === null) {
            return;
        }
        $filename = $meta->getOriginalFilename();
        if ($filename === null) {
            $filename = $file;
        }
        $matches = [];
        preg_match('/^' . $format . '$/', $filename, $matches);
        if (isset($matches['documentId']) && !empty($matches['documentId'])) {
            $meta->setDocumentIds([$this->getDocumentUri($matches['documentId'])]);
            if (strpos($matches['documentId'], 'MX.') === 0) {
                $meta->getIdentifications()->setTaxonIds([$matches['documentId']]);
            }
        } else {
            $taxa = $meta->getIdentifications()->getTaxonIds();
            if (empty($taxa)) {
                throw new \Exception("Image filename must contain identifier or you must specify the taxon ID!");
            }
        }
        if (isset($matches['capturers'])) {
            $meta->setCapturers(array($matches['capturers']));
        }
        // Pick tags from the filename
        $allOptions = (new Tags())->getValueOptions();
        $options = $meta->getTags();
        $lowerFilename = strtolower($filename);
        foreach ($allOptions as $key => $value) {
            if (strpos($lowerFilename, $key) !== false) {
                $options[] = $key;
            }
        }
        $meta->setTags($options);
    }

    protected function getDocumentUri($value) {
        if ($this->domainPrefixes === null || strpos($value, ':') === true) {
            return IdService::getUri($value);
        }
        $parts = explode('.', $value, 2);
        if (count($parts) == 2) {
            $ns = $parts[0];
        } else {
            $ns = '';
        }
        if (is_array($this->domainPrefixes) && isset($this->domainPrefixes[$ns])) {
            $value = rtrim($this->domainPrefixes[$ns], ':') . ':' . $value;
        }
        return IdService::getUri($value);
    }

    private function addJsonHeaders(Client $client)
    {
        $header = new Headers();
        $header->addHeaderLine('Content-Type', 'application/json');
        $client->setHeaders($header);
    }

    private function getHttpClient($url = null)
    {
        $options = [
            'adapter' => 'Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_CONNECTTIMEOUT => 30,// This is high because warming up the server might take sometime
                CURLOPT_TIMEOUT => 600
            ),
        ];
        if ($this->conf->getCert() !== null) {
            $options['curloptions'][CURLOPT_SSL_VERIFYPEER] = true;
            $options['curloptions'][CURLOPT_CAINFO] = $this->conf->getCert();
        } else {
            $options['curloptions'][CURLOPT_SSL_VERIFYPEER] = false;
        }
        $client = new Client($url, $options);
        $client->setAuth($this->conf->getUsername(), $this->conf->getPassword());
        return $client;
    }

} 