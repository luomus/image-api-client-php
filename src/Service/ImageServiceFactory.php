<?php

namespace ImageApi\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ImageServiceFactory implements FactoryInterface
{
    const CACHE_PREFIXES = '_DOMAIN_PREFIXES_';

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \ImageApi\Options\ImageServerConfiguration $configurations */
        $configurations = $serviceLocator->get('ImageApi\Service\Configuration');


        return new ImageService($configurations, $this->getDomainPrefixes($serviceLocator));
    }

    private function getDomainPrefixes(ServiceLocatorInterface $serviceLocator) {
        $config = $serviceLocator->get('Config');
        $nsWS = isset($config['externals']['namespaceWS']['url']) ? $config['externals']['namespaceWS']['url'] : null;
        if ($serviceLocator->has('cache')) {
            $cache = $serviceLocator->get('cache');
            if ($cache->hasItem(self::CACHE_PREFIXES)) {
                return $cache->getItem(self::CACHE_PREFIXES);
            }
            $data = $this->fetchNamespaces($nsWS);
            $cache->setItem(self::CACHE_PREFIXES, $data);
            return $data;
        }
        return $this->fetchNamespaces($nsWS);
    }

    private function fetchNamespaces($nsWS) {
        if ($nsWS === null) {
            return [];
        }
        $data = json_decode(file_get_contents($nsWS), true);
        $result = [];
        foreach($data as $ns) {
            if (!isset($ns['namespace_type']) || $ns['qname_prefix'] === 'all') {
                continue;
            }
            $result[trim($ns['namespace_id'])] = trim($ns['qname_prefix']);
        }
        return $result;
    }
}