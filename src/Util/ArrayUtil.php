<?php
namespace ImageApi\Util;


class ArrayUtil
{

    static function multi_implode($glue, $pieces)
    {
        $retVal = [];
        foreach ($pieces as $value) {
            if (is_array($value)) {
                $retVal[] = self::multi_implode($glue, $value);
            } else {
                $retVal[] = $value;
            }
        }
        return implode($glue, $retVal);
    }

} 